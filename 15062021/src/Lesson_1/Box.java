package Lesson_1;

import java.util.Arrays;
import java.util.Objects;

public class Box {

    private int[] mass = new int[]{1,2};

    public int[] getMass() {
        return mass;
    }

    public Box(String color, int weight, String name) {
        this.color = color;
        this.weight = weight;
        this.name = name;
    }

    public Box(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Box box = (Box) o;
//        return weight == box.weight && Arrays.equals(mass, box.mass) && Objects.equals(color, box.color) && Objects.equals(name, box.name);
//    }
//
//    @Override
//    public int hashCode() {
//        int result = Objects.hash(color, weight, name);
//        result = 31 * result + Arrays.hashCode(mass);
//        return result;
//    }

    String color;
    int weight;
    String name;

    public void info() {
        System.out.println(color + " " + weight + " " + name + " " + Arrays.toString(mass));
    }
}
