package Lesson_2;

public enum Fruit {
    ORANGE("Апельсин", 3), APPLE("яблоко", 4), BANANA("Банан", 5),
    CHERRY("Вишня", 1);

    private String rus;
    private int weight;

    Fruit(String rus, int weight) {
        this.rus = rus;
        this.weight = weight;
    }

    public String getRus() {
        return rus;
    }

    public int getWeight() {
        return weight;
    }
}

class MainEnum {
    public static void main(String[] args) {
//        Fruit f = Fruit.APPLE;
//
//        if (Fruit.APPLE == Fruit.BANANA) {
//
//        }

        for (Fruit o : Fruit.values()) {
            System.out.println(o + " " + o.getRus() + " " + o.getWeight() + " " + o.ordinal());
        }


    }
}