package Lesson_2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MainEx {
    public static void main(String[] args) {
//
//        int a = 0;
//        int b = 10 / a;
//
//        System.out.println("END");


//        try {
//            int a = 0;
//            int b = 10 / a;
//        } catch (ArithmeticException e) {
//            e.printStackTrace();
//        }
//
//
//        System.out.println("END!");

//

//        try {
//            int[] r = {1, 2, 3};
//            int a = 0;
//            r[20] = 20;
//            int b = 10 / a;
//        }
//        catch (ArithmeticException e) {
//            System.out.println("Возникло ArithmeticException");
//        }
//        catch (ArrayIndexOutOfBoundsException e) {
//            System.out.println("Выход за пределы массива");
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Возникло Исключение");
//        }
//////
//        finally {
//            System.out.println("Выполниться в любом случае!");
//        }


//        try {
//            System.out.println(sqrt(-10));
//        } catch (ArithmeticException e) {
//            e.printStackTrace();
//        }

//        try {
//            a();
//
//            // тут вызываются методы базы
//        } catch (FileNotFoundException e) {
//            // reconnect
//            e.printStackTrace();
//        }
//        System.out.println("end");

//        System.out.println(testFinally());

//        try {
//            a();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }


//        try {
//            connectToDb();
//
//            a();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        System.out.println(sqrt(-10));

    }

//    public static int testFinally() {
//        try {
//            return 1;
//        } finally {
//            return 2;
//        }
//    }

    public static void connectToDb() {
        System.out.println("Соединение с БД");
    }

    public static void a() throws FileNotFoundException {
        b();
    }

    public static void b() throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("1.txt");
    }


    //

    public static int sqrt(int n) {
        if (n > 0) {
            return n / 2;
        }
        throw new ArithmeticException("нельзя отрицательное!");
    }
}
