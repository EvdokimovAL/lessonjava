package Lesson_3;

import java.util.Comparator;
import java.util.TreeSet;

class Main {
    public static void main(String[] args) {

//        TreeSet<String> ts = new TreeSet<>();
//
//        ts.add("Z");
//        ts.add("B");
//        ts.add("G");
//        ts.add("A");
//
//        System.out.println(ts);



//        TreeSet<Employee> ts = new TreeSet();
//        ts.add(new Employee(20));
//        ts.add(new Employee(30));
//        ts.add(new Employee(10));
//        ts.add(new Employee(40));
//        System.out.println(ts);





//        TreeSet<Employee> ts = new TreeSet(new MyComp());
//        ts.add(new Employee(20, 20));
//        ts.add(new Employee(30, 25));
//        ts.add(new Employee(10, 30));
//        ts.add(new Employee(40, 35));
//        System.out.println(ts);
    }
}

class MyComp implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        if(o1.getSalary() > o2.getSalary()) {
            return 1;
        } else {
            return -1;
        }
    }
}

//class MyComp implements Comparator<Employee> {
//    @Override
//    public int compare(Employee o1, Employee o2) {
//        return o1.getSalary() - o2.getSalary();
//    }
//}





class Employee {
    private int salary;
    private int age;

    public Employee(int salary) {
        this.salary = salary;
    }

    public Employee(int salary, int age) {
        this.salary = salary;
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.valueOf(salary);
    }

    @Override
    public int hashCode() {
        return salary;
    }
}
