package Lesson_3;


public class Testr {
}

// 1 Что произойдет во время компиляции и выполнения данного кода?
//class MyThread extends Thread {
//    public void run() {
//        System.out.print("Running ");
//    }
//    public void start() {
//        System.out.print("Starting ");
//    }
//}
//
//class Q202 {
//    public static void main(String[] args) {
//        MyThread t = new MyThread();
//        t.start();
//    }
//}
//
//
//











// 2 Каким будет результат компиляции и запуска данного кода:
class Super {
    Super() {
        System.out.println("Super contructor");
    }
}

class Main1 extends Super {

    static int a;

    Main1() {
        this(1);
        System.out.println("Main() contructor");
    }
    Main1(int i) {
        System.out.println("Main(int) contructor");
    }
    public static void main(String [] args) {

        new Main1();
    }

}
//















// 3 Что напечатает следующий код:
//class Funcs extends java.lang.Math {
//
//    public int add(int x, int y) {
//        return x + y;
//    }
//    public int sub(int x, int y) {
//        return x - y;
//    }
//    public static void main(String[] a) {
//        Funcs f = new Funcs();
//        System.out.println("" + f.add(1, 2));
//    }
//}
//








// 4 Результат выполнения программы :
//class A {
//    public static void main(String[] args) {
//        Boolean f1 = true;
//        Boolean f2 = new Boolean("/false");
//        String a = "" + 1 + '+' + 1 + '=' + (1 + 1) + " is ";
//        String b = a + f1 + '/' + f2;
//        System.out.println(b);
//    }
//}
//
//






// Что произойдет при попытке запустить/откомпилировать следующий программный код?
//
//class A {
//    public static void main(String[] args){
//        B b1 = new B("one","two");
//        B b2 = new B("one", "two");
//        B b3 = b1;
//        System.out.println(b1 == b2);
//        System.out.println(b1 == b3);
//        System.out.println(b2 == b3);
//        System.out.println(b1.equals(b2));
//        System.out.println(b1.equals(b3));
//        System.out.println(b3.equals(b2));
//    }
//}
////
//class B {
//    public B(String prop1, String prop2){
//        this.prop1 = prop1;
//        this.prop2 = prop2;
//    }
//    private String prop1 = null;
//    private String prop2 = null;
//}
//
//




/////////


//class Test3 {
//    public static void main(String[] args) {
//        try {
//            int i = 5;
//        } catch (Exception e) {
//            System.out.print("catch");
//        } finally {
//            System.out.print("finally");
//        }
//    }
//}









class Tetst1 {
    public static void main(String[] args) {
        int i = 8;
        System.out.println(i++);
        System.out.println(i+1);
        System.out.println(i);
    }
}


















//
//class Task1 {
//    public static void main(String[] args) {
//        System.out.println(new Task1());
//    }
//}
//




















//
//class A {
//    {
//        System.out.println("dym a");
//    }
//
//    static {
//        System.out.println("stat a");
//    }
//
//    A(){
//        System.out.println("A");
//    }
//
//    public static void main(String[] args) {
//        new Lesson_1.A();
//    }
//}
//






























//class TestEquals {
//    public static void main(String[] args) {
//        String s1 = new String("Bicycle");
//        String s2 = new String("bicycle");
//        System.out.println(s1.equals(s2) == s2.equals(s1));
//    }
//}


































//
//class TestInc {
//    public static void main(String[] args) {
//        int x = 0;
//        System.out.print(x++==++x);
//    }
//}








// 1 --------------------------
//class Mountain {
//    static String name = "Himalaya";
//    static Lesson_1.Mountain getMountain() {
//        System.out.println("Getting Name ");
//        return null;
//    }
//    public static void main(String[ ] args) {
//        System.out.println( getMountain().name );
//    }
//}
// -----------------------------------














// 2 ---------------------
//class Test2 {
//    static void method(int... a) {
//        System.out.println("inside int...");
//    }
//    static void method(long a, long b) {
//        System.out.println("inside long");
//    }
//    static void method(Integer a, Integer b) {
//        System.out.println("inside INTEGER");
//    }
//    public static void main(String[] args) {
//        int a = 2;
//        int b = 3;
//        method(a,b);
//    }
//}
// ------------------------------




















// 3
//class Switch {
//    public static void main(String[] args) {
//        int i = 3;
//        switch(i) {
//            default:
//                System.out.println("default");
//            case 1:
//                System.out.println("1");
//            case 2:
//                System.out.println("2");
//        }
//    }
//}
















//4
//Перечислите все валидные сигнатуры конструкторов класса Clazz:
//
//        Clazz(String name)
//        Clazz Clazz(String name)
//        int Clazz(String name)
//        void Clazz(String name)
//        Clazz(name)
//        Clazz()




















// 5 вывести алфавит

class Test5 {
    public static void main(String[] args) {
        for (char i = 'а'; i <= 'я'; i++) {
            System.out.print((int)i + " ");
        }
    }
}



















//class TestStr {
//    public static void main(String[] args) {
//        String s = new String("ssssss");
//        StringBuffer sb = new StringBuffer("bbbbbb");
//        s.concat("-aaa");
//        sb.append("-aaa");
//        System.out.println(s);
//        System.out.println(sb);
//    }
//}