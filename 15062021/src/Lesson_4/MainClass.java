package Lesson_4;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainClass {

    public static void main(String[] args) {
        new MyWindow();
    }

}


//class MyWindow extends JFrame {
//    public MyWindow() {
//        setTitle("Java GUI");
//        setBounds(800, 300, 400, 500);

//        setLayout(null);
//
//        JButton button = new JButton("start");
//        button.setBounds(100,50,150,80);
//        add(button);

//        JButton button = new JButton("start");
//
//        button.setPreferredSize(new Dimension(50,150));
//        add(button, BorderLayout.SOUTH);
//
//        button.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Hello");
//            }
//        });
//
//
//        setVisible(true);
//    }
//}

//
class MyWindow extends JFrame {
    public MyWindow() {
        setTitle("Java GUI");
        setBounds(800, 300, 400, 500);

        JPanel bottomPanel = new JPanel();
        JPanel centerPanel = new JPanel();

        centerPanel.setBackground(Color.black.gray);
        bottomPanel.setBackground(Color.black.green);

        bottomPanel.setPreferredSize(new Dimension(1, 40));

        add(bottomPanel, BorderLayout.SOUTH);
        add(centerPanel, BorderLayout.CENTER);

        centerPanel.setLayout(new BorderLayout());
        bottomPanel.setLayout(new FlowLayout());



        JTextArea jta = new JTextArea();
        JScrollPane jsp = new JScrollPane(jta);
        centerPanel.add(jsp, BorderLayout.CENTER);

        JTextField jtf = new JTextField();

        bottomPanel.add(jtf);
        JButton jb = new JButton("Send");
        // addButtom(bottomPanel);
        bottomPanel.add(jb);

        jtf.setPreferredSize(new Dimension(300, 29));
        jta.setEditable(false);

        jb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jta.append(jtf.getText() + "\n");
                jtf.setText("");
                jtf.grabFocus();
            }
        });

//        jb.addKeyListener(new KeyAdapter() {
//            @Override
//            public void (KeyEvent e) {
//                //super.keyPressed(e);
//                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    jta.append(jtf.getText() + "\n");
//                    jtf.setText("");
//                    jtf.grabFocus();
//                }
//            }
//        });

//
//        jtf.addKeyListener(new KeyAdapter() {
//            @Override
//            public void keyReleased(KeyEvent e) {
//                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    jta.append(jtf.getText() + "\n");
//                    jtf.setText("");
//                    jtf.grabFocus();
//                }
//            }
//        });


        //setLayout(null);
//        JButton btn1 = new JButton("Ok");
//        JButton btn2 = new JButton("Cancel");
//        JPanel jPanel = new JPanel(new GridLayout(1,2));
//        jPanel.add(btn1);
//        jPanel.add(btn2);
//        add(jPanel, BorderLayout.SOUTH);
//
////        btn1.addActionListener((ActionEvent e) -> {
////            System.out.println("Ok");
////        }
//
//        int i = 10;
//
//        JPanel jPanel = new JPanel();
//        jPanel.setBackground(Color.RED);
//        add(jPanel, BorderLayout.CENTER);
//
//        jPanel.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseReleased(MouseEvent e) {
//                //super.mouseReleased(e);
//                System.out.println(e.getX() + " " + e.getY());
//            }
//        });

        //     jPanel.removeMouseListener();

//        btn1.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Ok" + i);
//            }
//        });
//
//        btn2.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Cancel");
//            }
//        });

        //  btn1.setBounds(50, 30, 100, 30);

        //    JButton btn2 = new JButton("Cancel");

        // btn1.setPreferredSize(new Dimension(10,300));

//        JPanel jPanel = new JPanel(new GridLayout(1,2));
//     //   jPanel.setLayout(null);
//        jPanel.setBounds(10,10, 150,150);
//        jPanel.setBackground(Color.red);
//
//        jPanel.add(btn1);
//       // jPanel.add(btn2);
//
//        add(jPanel, BorderLayout.SOUTH);

//        btn1.setBounds(15, 5, 85, 30);
//
//        JButton btn2 = new JButton("btn2");
//        btn2.setBounds(85, 25, 55, 40);
//        add(btn1);
//        add(btn2);


        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    void addButtom(JPanel bottomPanel) {
        JButton jb = new JButton("Send");
        bottomPanel.add(jb);

    }
}