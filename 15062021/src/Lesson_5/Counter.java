package Lesson_5;


public class Counter {
    private int c;

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public synchronized void inc() {
        c++;
    }

    public synchronized void dec() {
        c--;
    }

    public Counter() {
        this.c = 0;
    }
}

class MainCounter {
    public static void main(String[] args) {
        Counter counter = new Counter();

        long t = System.currentTimeMillis();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000000; i++) {
                    counter.inc();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000000; i++) {
                    counter.dec();
                }
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }



        System.out.println(counter.getC());
//        System.out.println(System.currentTimeMillis() - t);

//        Box box = new Box();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                box.doSomeThing();
//            }
//        }).start();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                box.doSomeThing();
//            }
//        }).start();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                box.doSomeThing();
//            }
//        }).start();


    }
}


class Box {
    Object object = new Object();

    void doSomeThing() {
        System.out.println(1 + " " + Thread.currentThread().getName());
        synchronized (object) {
            System.out.println(11 + " " + Thread.currentThread().getName());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(22 + " " + Thread.currentThread().getName());
        }
        System.out.println(2 + " " + Thread.currentThread().getName());
    }
}